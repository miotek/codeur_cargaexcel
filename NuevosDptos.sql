USE [CODEUR_PANEL_CONTROL]
GO

INSERT INTO [Tabla_Departamento]
           ([nombreDpto]
           ,[fechaAlta]
           ,[usuarioAlta]
           ,[fechaUltimaModif]
           ,[usuarioUltimaModif]
           ,[fechaBaja]
           ,[usuarioBaja])
     VALUES
           ('Limp y mtto jardines'
           ,2020-04-13
           ,'admin'
           ,null
           ,null
           ,null
           ,null)
GO


INSERT INTO [Tabla_Departamento]
           ([nombreDpto]
           ,[fechaAlta]
           ,[usuarioAlta]
           ,[fechaUltimaModif]
           ,[usuarioUltimaModif]
           ,[fechaBaja]
           ,[usuarioBaja])
     VALUES
           ('Ejecucion-Mtto'
           ,2020-04-13
           ,'admin'
           ,null
           ,null
           ,null
           ,null)
GO

INSERT INTO [Tabla_Departamento]
           ([nombreDpto]
           ,[fechaAlta]
           ,[usuarioAlta]
           ,[fechaUltimaModif]
           ,[usuarioUltimaModif]
           ,[fechaBaja]
           ,[usuarioBaja])
     VALUES
           ('Piscina'
           ,2020-04-13
           ,'admin'
           ,null
           ,null
           ,null
           ,null)
GO

INSERT INTO [Tabla_Departamento]
           ([nombreDpto]
           ,[fechaAlta]
           ,[usuarioAlta]
           ,[fechaUltimaModif]
           ,[usuarioUltimaModif]
           ,[fechaBaja]
           ,[usuarioBaja])
     VALUES
           ('CODEUR AGRO'
           ,2020-04-13
           ,'admin'
           ,null
           ,null
           ,null
           ,null)
GO

DBCC CHECKIDENT ('Tabla_Fichero', RESEED, 1)
DBCC CHECKIDENT ('Tabla_PresupuestoAnual', RESEED, 1)