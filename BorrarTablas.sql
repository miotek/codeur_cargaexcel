USE [CODEUR_PANEL_CONTROL]
go

delete
FROM [dbo].[Tabla_PresupuestoMensual]
go

DBCC CHECKIDENT ('Tabla_PresupuestoMensual', RESEED, 0)
go

delete
FROM [dbo].[Tabla_PresupuestoAnual]
go

DBCC CHECKIDENT ('Tabla_PresupuestoAnual', RESEED, 0)
go

delete
FROM [dbo].[Tabla_Matriz]
go

DBCC CHECKIDENT ('Tabla_Matriz', RESEED, 0)
go

delete
  FROM [dbo].[Tabla_Fichero]
go

DBCC CHECKIDENT ('Tabla_Fichero', RESEED, 0)
go
