﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODEURCargaExcelLIB
{
    public class trabajador
    {
        #region PROPIEDADES

        public long ID { get; set; }

        public string Nombre { get; set; }

        public bool EnOficina { get; set; }

        #endregion
    }
}
