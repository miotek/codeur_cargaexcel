﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using ConexionBBDD;
using System.Web;
using System.Security.Principal;
using System.Configuration;
using System.IO;

namespace CODEURCargaExcelLIB
{
    public class interopExcel: IDisposable
    {
        #region CONSTANTES

        const int COL_COD_CTA_FINANCIERA = 1;
        const int COL_COD_CTA_ANALITICA = 2;
        const int COL_CONCEPTO = 5;
        const int COL_PRESUP_ANUAL = 7;
        const int COL_REAL_ANNIO_ANT = 11;

        const int COL_ENE = 15;
        const int COL_FEB = 16;
        const int COL_MAR = 17;
        const int COL_ABR = 18;
        const int COL_MAY = 19;
        const int COL_JUN = 20;
        const int COL_JUL = 21;
        const int COL_AGO = 22;
        const int COL_SEP = 23;
        const int COL_OCT = 24;
        const int COL_NOV = 25;
        const int COL_DIC = 26;

        #endregion

        Application _objExcel;
        Workbook _objLibro;

        #region PROPIEDADES

        public long IDFichero { get; set; }

        public string NombreFicheroExcel { get; set; } // Sólo Nombre

        public string FicheroExcel { get; set; } // Nombre + Ruta completa

        public int Annio { get; set; }

        private Exception _objERROR;
        public Exception ERROR
        {
            get { return _objERROR; }
            set { _objERROR = value; }
        }

        private List<hoja> _lstHojasExcel;
        public List<hoja> HojasExcel
        {
            get { return _lstHojasExcel; }
            set { _lstHojasExcel = value; }
        }

        private matriz _objMatriz;

        public matriz Matriz
        {
            get { return _objMatriz; }
            set { _objMatriz = value; }
        }


        #endregion

        public interopExcel(int piAnnio, string pstrFicheroExcel)
        {
            FicheroExcel = pstrFicheroExcel;
            Annio = piAnnio;
        }

        #region CARGA DATOS DE EXCEL
        
        public bool AbrirExcel()
        {
            bool bResultado = true;
            try
            {
                _objExcel = new Application();
                _objExcel.Visible = true;

                FileInfo objFichero = new FileInfo(FicheroExcel);
                NombreFicheroExcel = objFichero.Name;

                _objLibro = _objExcel.Workbooks.Open(FicheroExcel, 0);
            }
            catch (Exception exError)
            {
                ERROR = exError;
                bResultado = false;
            }

            return bResultado;
        }

        public bool CerrarExcel()
        {
            bool bResultado = true;
            try
            {
                _objLibro.Close();

                _objExcel.Quit();
                _objExcel = null;
            }
            catch (Exception exError)
            {
                ERROR = exError;
                bResultado = false;
            }

            return bResultado;
        }

        public bool CargaExcelPresupuesto()
        {
            linea objLinea;
            hoja objHoja;

            bool bResultado = true;

            try
            {
                Dictionary<int, string> dicDepartamentos = GetDepartamentos();

                _lstHojasExcel = new List<hoja>();

                foreach (Worksheet objHojaExcel in _objLibro.Sheets)
                {
                    
                    if (dicDepartamentos.ContainsValue(objHojaExcel.Name.ToString().Trim()))
                    {
                        KeyValuePair<int,string> objDpto = dicDepartamentos.Where(X => X.Value == objHojaExcel.Name.ToString().Trim()).FirstOrDefault();
                        objHoja = new hoja(objDpto.Value.Trim(), objDpto.Key);
                        
                        #region Obtener ultima fila y columna

                        Range objUltima = objHojaExcel.Cells.SpecialCells(XlCellType.xlCellTypeLastCell, Type.Missing);
                        Range objRango = objHojaExcel.get_Range("A1", objUltima);

                        int iUltimaFila = objUltima.Row;
                        int iUltimaColumna = objUltima.Column;

                        #endregion

                        for (int iFila = 1; iFila < iUltimaFila; iFila++)
                        {                           
                            bool bTieneCtaAnalitica = (objHojaExcel.Cells[iFila, COL_COD_CTA_ANALITICA].Value2 != null);
                            bool bTieneConcepto = (objHojaExcel.Cells[iFila, COL_CONCEPTO].Value2 != null);
                            bool bTienePptoAnual = (objHojaExcel.Cells[iFila, COL_PRESUP_ANUAL].Value2 != null);
                            
                            if(bTieneConcepto && bTienePptoAnual)
                            {
                                objLinea = new linea();

                                if (!bTieneCtaAnalitica) objLinea.EsTotal = true;

                                objLinea.CtaFinanciera = objHojaExcel.Cells[iFila, COL_COD_CTA_FINANCIERA].Text.ToString();
                                objLinea.CtaAnalitica = objHojaExcel.Cells[iFila, COL_COD_CTA_ANALITICA].Text.ToString();
                                objLinea.Concepto = objHojaExcel.Cells[iFila, COL_CONCEPTO].Text.ToString();

                                if (objHojaExcel.Cells[iFila, COL_PRESUP_ANUAL].Value2 != null)
                                    objLinea.PresupuestoAnnio = (double)(objHojaExcel.Cells[iFila, COL_PRESUP_ANUAL] as Range).Value;

                                if (objHojaExcel.Cells[iFila, COL_REAL_ANNIO_ANT].Value2 != null)
                                    objLinea.RealAnnioAnt = (double)(objHojaExcel.Cells[iFila, COL_REAL_ANNIO_ANT] as Range).Value;

                                if (objHojaExcel.Cells[iFila, COL_ENE].Value2 != null)
                                    objLinea.Presup_ENE = (double)(objHojaExcel.Cells[iFila, COL_ENE] as Range).Value;

                                if (objHojaExcel.Cells[iFila, COL_FEB].Value2 != null)
                                    objLinea.Presup_FEB = (double)(objHojaExcel.Cells[iFila, COL_FEB] as Range).Value;

                                if (objHojaExcel.Cells[iFila, COL_MAR].Value2 != null)
                                    objLinea.Presup_MAR = (double)(objHojaExcel.Cells[iFila, COL_MAR] as Range).Value;

                                if (objHojaExcel.Cells[iFila, COL_ABR].Value2 != null)
                                    objLinea.Presup_ABR = (double)(objHojaExcel.Cells[iFila, COL_ABR] as Range).Value;

                                if (objHojaExcel.Cells[iFila, COL_MAY].Value2 != null)
                                    objLinea.Presup_MAY = (double)(objHojaExcel.Cells[iFila, COL_MAY] as Range).Value;

                                if (objHojaExcel.Cells[iFila, COL_JUN].Value2 != null)
                                    objLinea.Presup_JUN = (double)(objHojaExcel.Cells[iFila, COL_JUN] as Range).Value;

                                if (objHojaExcel.Cells[iFila, COL_JUL].Value2 != null)
                                    objLinea.Presup_JUL = (double)(objHojaExcel.Cells[iFila, COL_JUL] as Range).Value;

                                if (objHojaExcel.Cells[iFila, COL_AGO].Value2 != null)
                                    objLinea.Presup_AGO = (double)(objHojaExcel.Cells[iFila, COL_AGO] as Range).Value;

                                if (objHojaExcel.Cells[iFila, COL_SEP].Value2 != null)
                                    objLinea.Presup_SEP = (double)(objHojaExcel.Cells[iFila, COL_SEP] as Range).Value;

                                if (objHojaExcel.Cells[iFila, COL_OCT].Value2 != null)
                                    objLinea.Presup_OCT = (double)(objHojaExcel.Cells[iFila, COL_OCT] as Range).Value;

                                if (objHojaExcel.Cells[iFila, COL_NOV].Value2 != null)
                                    objLinea.Presup_NOV = (double)(objHojaExcel.Cells[iFila, COL_NOV] as Range).Value;

                                if (objHojaExcel.Cells[iFila, COL_DIC].Value2 != null)
                                    objLinea.Presup_DIC = (double)(objHojaExcel.Cells[iFila, COL_DIC] as Range).Value;

                                objHoja.Lineas.Add(objLinea);
                            }
                        }

                        _lstHojasExcel.Add(objHoja);
                    }                                        
                }
            }
            catch (Exception exError)
            {
                bResultado = false;
                //LOGFile.WriteLogFile(CLASS_NAME, FUNCION, exError.Message, utilidades.Usuario, utilidades.CarpetaErrores);
            }

            return bResultado;
        }

        public bool CargaExcelMatrices()
        {
            const string HOJA_MATRICES = "Matrices";

            bool bResultado = true;
            int iFilaCabeceras = 1;


            try
            {
                Dictionary<int, string> dicCabecerasColumna = null;
                Dictionary<int, string> dicDepartamentos = GetDepartamentos(true);

                Worksheet objHojaExcel = null;

                foreach (Worksheet objHojaExcelMatrices in _objLibro.Sheets)
                {
                    if (objHojaExcelMatrices.Name == HOJA_MATRICES)
                    {
                        objHojaExcel = objHojaExcelMatrices;
                        break;
                    }
                }

                if (objHojaExcel != null)
                {
                    // Genera el objeto Matriz
                    _objMatriz = new matriz();

                    #region Obtener ultima fila y columna

                    Range objUltima = objHojaExcel.Cells.SpecialCells(XlCellType.xlCellTypeLastCell, Type.Missing);
                    Range objRango = objHojaExcel.get_Range("A1", objUltima);

                    int iUltimaFila = objUltima.Row;
                    int iUltimaColumna = objUltima.Column;

                    #endregion

                    List<trabajador> lstTrabajadores = GetTrabajadores();

                    bool bHayCabecerasDeColumnas = false;

                    for (int iFila = 1; iFila < iUltimaFila; iFila++)
                    {
                        // Averigua cual es la fila donde están las cabeceras de las columnas y rellena la colección
                        string strCelda = (string)(objHojaExcel.Cells[iFila, 1] as Range).Value;
                        if (strCelda != null && strCelda.Split().Length > 0 && strCelda.Trim().ToUpper() == "ETIQUETAS DE FILA")
                        {
                            iFilaCabeceras = iFila;
                            bHayCabecerasDeColumnas = true;
                            dicCabecerasColumna = new Dictionary<int, string>();
                            int iColCabecera = 2;

                            strCelda = (string)(objHojaExcel.Cells[iFila, iColCabecera] as Range).Value;
                            while (strCelda.Trim().ToUpper() != "TOTAL GENERAL" && strCelda != null)
                            {                                
                                dicCabecerasColumna.Add(iColCabecera, strCelda);
                                ++iColCabecera;
                                strCelda = (string)(objHojaExcel.Cells[iFila, iColCabecera] as Range).Value;
                            }

                            iUltimaColumna = iColCabecera - 1;
                        }
                        else
                        {
                            if (bHayCabecerasDeColumnas)
                            {
                                // En el momento en que aparezca el texto "Total General" en la celda, parará el bucle
                                if (strCelda != null && strCelda.Split().Length > 0 && strCelda.Trim().ToUpper() != "TOTAL GENERAL")
                                {
                                    // Comprueba que el contenido de la celda se corresponde con un trabajador
                                    trabajador objTrabajador = lstTrabajadores.Where(X => QuitaAcentos(X.Nombre) == QuitaAcentos(strCelda)).FirstOrDefault();

                                    if (objTrabajador != null)
                                    {
                                        // Crea el objeto MatrizPersona
                                        matrizPersona objMatrizPersona = new matrizPersona();

                                        objMatrizPersona.IDPersona = objTrabajador.ID;
                                        objMatrizPersona.ApellidosNombre = objTrabajador.Nombre;

                                        for (int iCol = 2; iCol <= iUltimaColumna; iCol++)
                                        {
                                            if (objHojaExcel.Cells[iFila, iCol].Value2 != null)
                                            {
                                                // Obtiene el ID del Departamento ubicado en esa columna
                                                // 1. Obtiene el nombre de la cabecera
                                                string strCabecera = dicCabecerasColumna[iCol]; //(string)(objHojaExcel.Cells[iFilaCabeceras, iCol] as Range).Value;

                                                // 2. Localiza el ID en el diccionario de cabeceras
                                                KeyValuePair<int, string> objKeyValuePair = dicDepartamentos.Where(X => X.Value.Trim().ToUpper() == strCabecera.Trim().ToUpper()).FirstOrDefault();
                                                if (!objKeyValuePair.Equals(default(KeyValuePair<int, string>)))
                                                    objMatrizPersona.ValoresPorDpto.Add(objKeyValuePair.Key, (double)(objHojaExcel.Cells[iFila, iCol] as Range).Value);
                                                else
                                                {
                                                    throw new Exception("Departamento inexistente");
                                                }
                                            }
                                        }
                                        _objMatriz.MatrizPersonasDpto.Add(objMatrizPersona);
                                    }                                   
                                }
                                else if (strCelda.Trim().ToUpper() == "TOTAL GENERAL")
                                {
                                    break;
                                }
                            }
                        }
                    }

                    //_lstHojasExcel.Add(objHoja);
                }
            }
            catch (Exception exError)
            {
                ERROR = exError;
                bResultado = false;
                //LOGFile.WriteLogFile(CLASS_NAME, FUNCION, exError.Message, utilidades.Usuario, utilidades.CarpetaErrores);
            }

            return bResultado;
        }

        #endregion

        public bool InsertaDatosExcelEnBBDD(bool pbReemplazarExistente, bool pbCargarMatrices = false)
        {
            string strSQL;
            bool bResultado = true;
            long lIDFichero = 0;
            List<parametroBBDD> lstParametros;
            string strUsuario = GetUsuarioConectado();

            string strCadenaConexion = ConfigurationManager.ConnectionStrings["CODEUR"].ConnectionString;

            string strTabla = ConfigurationManager.AppSettings["TABLA_PRESUPUESTOS"].Trim();
            string strPK = ConfigurationManager.AppSettings["TABLA_PRESUPUESTOS_PK"].Trim();

            string strTablaMes = ConfigurationManager.AppSettings["TABLA_PRESUPUESTOS_MES"].Trim();
            string strTablaMesPK = ConfigurationManager.AppSettings["TABLA_PRESUPUESTOS_MES_PK"].Trim();

            string strTablaMatriz = ConfigurationManager.AppSettings["TABLA_MATRIZ"].Trim();
            string strPKMatriz = ConfigurationManager.AppSettings["TABLA_MATRIZ_PK"].Trim();

            using (conexionSQLClient objBBDD = new conexionSQLClient(strUsuario, strCadenaConexion))
            {
                #region BORRAR DE FORMA lÓGICA FICHERO EXISTENTE

                long lIDFicheroExistente = 0;
                bool bActualizar = false;

                if (pbReemplazarExistente)
                {

                    #region Obtiene el ID del fichero existente

                    strSQL = string.Format("SELECT * FROM Tabla_Fichero WHERE Annio = {0} AND fechaBaja IS NULL", Annio);

                    System.Data.DataTable dtableFicheros = objBBDD.LoadTable(strSQL);

                    if (dtableFicheros.Rows.Count > 0)
                    {
                        lIDFicheroExistente = (long)dtableFicheros.Rows[0]["idFichero"];
                        IDFichero = lIDFicheroExistente;
                        bActualizar = lIDFicheroExistente > 0;
                    }

                    #endregion

                    if (bActualizar)
                    {
                        bResultado = objBBDD.InitTransaction();

                        try
                        {
                            #region Update para marcar el fichero como borrado

                            if (bResultado)
                            {
                                strSQL = "UPDATE Tabla_Fichero " +
                                         "SET " +
                                         "fechaBaja = @fechaBaja, " +
                                         "usuarioBaja = @usuarioBaja " +
                                         "WHERE IdFichero = @IdFichero";

                                lstParametros = new List<parametroBBDD>();

                                lstParametros.Add(new parametroBBDD() { Nombre = "@IdFichero", Valor = lIDFicheroExistente });
                                lstParametros.Add(new parametroBBDD() { Nombre = "@fechaBaja", Valor = DateTime.Now });
                                lstParametros.Add(new parametroBBDD() { Nombre = "@usuarioBaja", Valor = strUsuario });

                                int iResultado = objBBDD.ExecuteSQL(strSQL, lstParametros);
                                bResultado = (iResultado > 0);
                            }

                            #endregion

                            #region Update las líneas de presupuesto para marcarlas como borrado

                            #region PRESUPUESTO ANUAL
                            
                            if (bResultado)
                            {
                                strSQL = "UPDATE Tabla_PresupuestoAnual " +
                                         "SET " +
                                         "fechaBaja = @fechaBaja, " +
                                         "usuarioBaja = @usuarioBaja " +
                                         "WHERE IdFichero = @IdFichero";

                                lstParametros = new List<parametroBBDD>();

                                lstParametros.Add(new parametroBBDD() { Nombre = "@IdFichero", Valor = lIDFicheroExistente });
                                lstParametros.Add(new parametroBBDD() { Nombre = "@fechaBaja", Valor = DateTime.Now });
                                lstParametros.Add(new parametroBBDD() { Nombre = "@usuarioBaja", Valor = strUsuario });

                                int iResultado = objBBDD.ExecuteSQL(strSQL, lstParametros);
                                bResultado = (iResultado > 0);
                            }

                            #endregion

                            #region PRESUPUESTO MENSUAL                            

                            if (bResultado)
                            {
                                strSQL = "UPDATE Tabla_PresupuestoMensual " +
                                         "SET " +
                                         "fechaBaja = @fechaBaja, " +
                                         "usuarioBaja = @usuarioBaja " +
                                         "WHERE IdFichero = @IdFichero";

                                lstParametros = new List<parametroBBDD>();

                                lstParametros.Add(new parametroBBDD() { Nombre = "@IdFichero", Valor = lIDFicheroExistente });
                                lstParametros.Add(new parametroBBDD() { Nombre = "@fechaBaja", Valor = DateTime.Now });
                                lstParametros.Add(new parametroBBDD() { Nombre = "@usuarioBaja", Valor = strUsuario });

                                int iResultado = objBBDD.ExecuteSQL(strSQL, lstParametros);
                                bResultado = (iResultado >= 0);
                            }

                            #endregion

                            #endregion

                            #region Update las líneas de las matrices para marcarlas como borrado

                            if (bResultado)
                            {
                                strSQL = "UPDATE Tabla_Matriz " +
                                         "SET " +
                                         "fechaBaja = @fechaBaja, " +
                                         "usuarioBaja = @usuarioBaja " +
                                         "WHERE IdFichero = @IdFichero";

                                lstParametros = new List<parametroBBDD>();

                                lstParametros.Add(new parametroBBDD() { Nombre = "@IdFichero", Valor = lIDFicheroExistente });
                                lstParametros.Add(new parametroBBDD() { Nombre = "@fechaBaja", Valor = DateTime.Now });
                                lstParametros.Add(new parametroBBDD() { Nombre = "@usuarioBaja", Valor = strUsuario });

                                int iResultado = objBBDD.ExecuteSQL(strSQL, lstParametros);
                                bResultado = (iResultado >= 0);
                            }

                            #endregion
                        }
                        catch (Exception exError)
                        {
                            ERROR = exError;
                            bResultado = false;
                        }
                        finally
                        {
                            if (bResultado)
                                objBBDD.CommitTransaction();
                            else
                                objBBDD.RollbackTransaction();
                        }
                    }
                }

                #endregion

                if (bResultado)
                {
                    bResultado = objBBDD.InitTransaction();

                    try
                    {
                        if (_lstHojasExcel != null && _lstHojasExcel.Count > 0)
                        {
                            #region INSERTA EL FICHERO

                            strSQL = "INSERT INTO Tabla_Fichero " +
                                     "(nombreFichero, Annio, fechaAlta, usuarioAlta, fechaUltimaModif, usuarioUltimaModif, fechaBaja, usuarioBaja) " +
                                     "VALUES " +
                                     "(@nombreFichero, @Annio, GETDATE(), @usuarioAlta, NULL, NULL, NULL, NULL)";

                            lstParametros = new List<parametroBBDD>();

                            lstParametros.Add(new parametroBBDD() { Nombre = "@nombreFichero", Valor = NombreFicheroExcel });
                            lstParametros.Add(new parametroBBDD() { Nombre = "@Annio", Valor = Annio });
                            lstParametros.Add(new parametroBBDD() { Nombre = "@usuarioAlta", Valor = strUsuario });

                            int iResultado = objBBDD.ExecuteSQL(strSQL, lstParametros);
                            bResultado = (iResultado > 0);

                            if (bResultado)
                            {
                                // Recuperar el Identity del fichero insertado
                                strSQL = "SELECT IDENT_CURRENT('Tabla_Fichero')";

                                Object Objeto = objBBDD.ExecuteEscalar(strSQL);
                                lIDFichero = Convert.ToInt64(Objeto);

                                if (lIDFichero <= 0) bResultado = false;
                            }

                            #endregion

                            if (bResultado)
                            {
                                System.Data.DataTable dtableLineasPresup = null;
                                System.Data.DataTable dtableLineasPresupMes = null;

                                #region INSERTAR TABLAS CON BULKINSERT

                                #region RELLENA LA TABLA DE PRESUPUESTOS

                                dtableLineasPresup = objBBDD.CrearEstructuraTabla(strTabla, strPK);
                                dtableLineasPresupMes = objBBDD.CrearEstructuraTabla(strTablaMes, strTablaMesPK);

                                // Llenamos la tabla con la lista de líneas de presupuestos
                                foreach (hoja objHoja in _lstHojasExcel)
                                {                                    
                                    for (int iCnt = 0; iCnt < objHoja.Lineas.Count; iCnt++)
                                    {
                                        #region PRESUPUESTOS ANUALES
                                        
                                        DataRow drowFila = dtableLineasPresup.NewRow();

                                        string strGUID = Guid.NewGuid().ToString();                                   

                                        //drowFila["IdPresupuestoAnual"] LA PK al ser un Identity no lo incluimos
                                        drowFila["GUID"] = strGUID;
                                        drowFila["EsTotal"] = objHoja.Lineas[iCnt].EsTotal ? 1 : 0;
                                        drowFila["IdFichero"] = lIDFichero;
                                        drowFila["IdDepartamento"] = objHoja.IdDpto;
                                        drowFila["CtaFinanciera"] = objHoja.Lineas[iCnt].CtaFinanciera;
                                        drowFila["CtaAnalitica"] = objHoja.Lineas[iCnt].CtaAnalitica.Replace("-", "");
                                        drowFila["Concepto"] = objHoja.Lineas[iCnt].Concepto;
                                        drowFila["PresupuestoAnual"] = objHoja.Lineas[iCnt].PresupuestoAnnio;
                                        drowFila["RealAnnioAnt"] = objHoja.Lineas[iCnt].RealAnnioAnt;
                                        drowFila["fechaAlta"] = DateTime.Now;
                                        drowFila["usuarioAlta"] = strUsuario;
                                        drowFila["fechaUltimaModif"] = DBNull.Value;
                                        drowFila["usuarioUltimaModif"] = DBNull.Value;
                                        drowFila["fechaBaja"] = DBNull.Value;
                                        drowFila["usuarioBaja"] = DBNull.Value;
                                       
                                        dtableLineasPresup.Rows.Add(drowFila);

                                        #endregion

                                        #region PRESUPUESTOS MENSUALES

                                        DataRow drowFilaMes = dtableLineasPresupMes.NewRow();

                                        //drowFila["IdPresupuestoMensual"] LA PK al ser un Identity no lo incluimos                                        
                                        drowFilaMes["IdFichero"] = lIDFichero;
                                        drowFilaMes["Fecha"] = DateTime.Parse("31/01/" + Annio);
                                        drowFilaMes["Presup_Mes"] = objHoja.Lineas[iCnt].Presup_ENE;
                                        drowFilaMes["GUID"] = strGUID;
                                        drowFilaMes["fechaAlta"] = DateTime.Now;
                                        drowFilaMes["usuarioAlta"] = strUsuario;
                                        drowFilaMes["fechaUltimaModif"] = DBNull.Value;
                                        drowFilaMes["usuarioUltimaModif"] = DBNull.Value;
                                        drowFilaMes["fechaBaja"] = DBNull.Value;
                                        drowFilaMes["usuarioBaja"] = DBNull.Value;

                                        dtableLineasPresupMes.Rows.Add(drowFilaMes);

                                        drowFilaMes = dtableLineasPresupMes.NewRow();
                                        
                                        drowFilaMes["IdFichero"] = lIDFichero;
                                        drowFilaMes["Fecha"] = DateTime.Parse("28/02/" + Annio);
                                        drowFilaMes["Presup_Mes"] = objHoja.Lineas[iCnt].Presup_FEB;
                                        drowFilaMes["GUID"] = strGUID;
                                        drowFilaMes["fechaAlta"] = DateTime.Now;
                                        drowFilaMes["usuarioAlta"] = strUsuario;
                                        drowFilaMes["fechaUltimaModif"] = DBNull.Value;
                                        drowFilaMes["usuarioUltimaModif"] = DBNull.Value;
                                        drowFilaMes["fechaBaja"] = DBNull.Value;
                                        drowFilaMes["usuarioBaja"] = DBNull.Value;

                                        dtableLineasPresupMes.Rows.Add(drowFilaMes);

                                        drowFilaMes = dtableLineasPresupMes.NewRow();
                                        
                                        drowFilaMes["IdFichero"] = lIDFichero;
                                        drowFilaMes["Fecha"] = DateTime.Parse("31/03/" + Annio);
                                        drowFilaMes["Presup_Mes"] = objHoja.Lineas[iCnt].Presup_MAR;
                                        drowFilaMes["GUID"] = strGUID;
                                        drowFilaMes["fechaAlta"] = DateTime.Now;
                                        drowFilaMes["usuarioAlta"] = strUsuario;
                                        drowFilaMes["fechaUltimaModif"] = DBNull.Value;
                                        drowFilaMes["usuarioUltimaModif"] = DBNull.Value;
                                        drowFilaMes["fechaBaja"] = DBNull.Value;
                                        drowFilaMes["usuarioBaja"] = DBNull.Value;

                                        dtableLineasPresupMes.Rows.Add(drowFilaMes);

                                        drowFilaMes = dtableLineasPresupMes.NewRow();
                                        
                                        drowFilaMes["IdFichero"] = lIDFichero;
                                        drowFilaMes["Fecha"] = DateTime.Parse("30/04/" + Annio);
                                        drowFilaMes["Presup_Mes"] = objHoja.Lineas[iCnt].Presup_ABR;
                                        drowFilaMes["GUID"] = strGUID;
                                        drowFilaMes["fechaAlta"] = DateTime.Now;
                                        drowFilaMes["usuarioAlta"] = strUsuario;
                                        drowFilaMes["fechaUltimaModif"] = DBNull.Value;
                                        drowFilaMes["usuarioUltimaModif"] = DBNull.Value;
                                        drowFilaMes["fechaBaja"] = DBNull.Value;
                                        drowFilaMes["usuarioBaja"] = DBNull.Value;

                                        dtableLineasPresupMes.Rows.Add(drowFilaMes);

                                        drowFilaMes = dtableLineasPresupMes.NewRow();
                                        
                                        drowFilaMes["IdFichero"] = lIDFichero;
                                        drowFilaMes["Fecha"] = DateTime.Parse("31/05/" + Annio);
                                        drowFilaMes["Presup_Mes"] = objHoja.Lineas[iCnt].Presup_MAY;
                                        drowFilaMes["GUID"] = strGUID;
                                        drowFilaMes["fechaAlta"] = DateTime.Now;
                                        drowFilaMes["usuarioAlta"] = strUsuario;
                                        drowFilaMes["fechaUltimaModif"] = DBNull.Value;
                                        drowFilaMes["usuarioUltimaModif"] = DBNull.Value;
                                        drowFilaMes["fechaBaja"] = DBNull.Value;
                                        drowFilaMes["usuarioBaja"] = DBNull.Value;

                                        dtableLineasPresupMes.Rows.Add(drowFilaMes);
                                        
                                        drowFilaMes = dtableLineasPresupMes.NewRow();
                                        
                                        drowFilaMes["IdFichero"] = lIDFichero;
                                        drowFilaMes["Fecha"] = DateTime.Parse("30/06/" + Annio);
                                        drowFilaMes["Presup_Mes"] = objHoja.Lineas[iCnt].Presup_JUN;
                                        drowFilaMes["GUID"] = strGUID;
                                        drowFilaMes["fechaAlta"] = DateTime.Now;
                                        drowFilaMes["usuarioAlta"] = strUsuario;
                                        drowFilaMes["fechaUltimaModif"] = DBNull.Value;
                                        drowFilaMes["usuarioUltimaModif"] = DBNull.Value;
                                        drowFilaMes["fechaBaja"] = DBNull.Value;
                                        drowFilaMes["usuarioBaja"] = DBNull.Value;

                                        dtableLineasPresupMes.Rows.Add(drowFilaMes);

                                        drowFilaMes = dtableLineasPresupMes.NewRow();
                                        
                                        drowFilaMes["IdFichero"] = lIDFichero;
                                        drowFilaMes["Fecha"] = DateTime.Parse("31/07/" + Annio);
                                        drowFilaMes["Presup_Mes"] = objHoja.Lineas[iCnt].Presup_JUL;
                                        drowFilaMes["GUID"] = strGUID;
                                        drowFilaMes["fechaAlta"] = DateTime.Now;
                                        drowFilaMes["usuarioAlta"] = strUsuario;
                                        drowFilaMes["fechaUltimaModif"] = DBNull.Value;
                                        drowFilaMes["usuarioUltimaModif"] = DBNull.Value;
                                        drowFilaMes["fechaBaja"] = DBNull.Value;
                                        drowFilaMes["usuarioBaja"] = DBNull.Value;

                                        dtableLineasPresupMes.Rows.Add(drowFilaMes);

                                        drowFilaMes = dtableLineasPresupMes.NewRow();
                                        
                                        drowFilaMes["IdFichero"] = lIDFichero;
                                        drowFilaMes["Fecha"] = DateTime.Parse("31/08/" + Annio);
                                        drowFilaMes["Presup_Mes"] = objHoja.Lineas[iCnt].Presup_AGO;
                                        drowFilaMes["GUID"] = strGUID;
                                        drowFilaMes["fechaAlta"] = DateTime.Now;
                                        drowFilaMes["usuarioAlta"] = strUsuario;
                                        drowFilaMes["fechaUltimaModif"] = DBNull.Value;
                                        drowFilaMes["usuarioUltimaModif"] = DBNull.Value;
                                        drowFilaMes["fechaBaja"] = DBNull.Value;
                                        drowFilaMes["usuarioBaja"] = DBNull.Value;

                                        dtableLineasPresupMes.Rows.Add(drowFilaMes);

                                        drowFilaMes = dtableLineasPresupMes.NewRow();
                                        
                                        drowFilaMes["IdFichero"] = lIDFichero;
                                        drowFilaMes["Fecha"] = DateTime.Parse("30/09/" + Annio);
                                        drowFilaMes["Presup_Mes"] = objHoja.Lineas[iCnt].Presup_SEP;
                                        drowFilaMes["GUID"] = strGUID;
                                        drowFilaMes["fechaAlta"] = DateTime.Now;
                                        drowFilaMes["usuarioAlta"] = strUsuario;
                                        drowFilaMes["fechaUltimaModif"] = DBNull.Value;
                                        drowFilaMes["usuarioUltimaModif"] = DBNull.Value;
                                        drowFilaMes["fechaBaja"] = DBNull.Value;
                                        drowFilaMes["usuarioBaja"] = DBNull.Value;

                                        dtableLineasPresupMes.Rows.Add(drowFilaMes);

                                        drowFilaMes = dtableLineasPresupMes.NewRow();
                                        
                                        drowFilaMes["IdFichero"] = lIDFichero;
                                        drowFilaMes["Fecha"] = DateTime.Parse("31/10/" + Annio);
                                        drowFilaMes["Presup_Mes"] = objHoja.Lineas[iCnt].Presup_OCT;
                                        drowFilaMes["GUID"] = strGUID;
                                        drowFilaMes["fechaAlta"] = DateTime.Now;
                                        drowFilaMes["usuarioAlta"] = strUsuario;
                                        drowFilaMes["fechaUltimaModif"] = DBNull.Value;
                                        drowFilaMes["usuarioUltimaModif"] = DBNull.Value;
                                        drowFilaMes["fechaBaja"] = DBNull.Value;
                                        drowFilaMes["usuarioBaja"] = DBNull.Value;

                                        dtableLineasPresupMes.Rows.Add(drowFilaMes);

                                        drowFilaMes = dtableLineasPresupMes.NewRow();
                                        
                                        drowFilaMes["IdFichero"] = lIDFichero;
                                        drowFilaMes["Fecha"] = DateTime.Parse("30/11/" + Annio);
                                        drowFilaMes["Presup_Mes"] = objHoja.Lineas[iCnt].Presup_NOV;
                                        drowFilaMes["GUID"] = strGUID;
                                        drowFilaMes["fechaAlta"] = DateTime.Now;
                                        drowFilaMes["usuarioAlta"] = strUsuario;
                                        drowFilaMes["fechaUltimaModif"] = DBNull.Value;
                                        drowFilaMes["usuarioUltimaModif"] = DBNull.Value;
                                        drowFilaMes["fechaBaja"] = DBNull.Value;
                                        drowFilaMes["usuarioBaja"] = DBNull.Value;

                                        dtableLineasPresupMes.Rows.Add(drowFilaMes);

                                        drowFilaMes = dtableLineasPresupMes.NewRow();
                                        
                                        drowFilaMes["IdFichero"] = lIDFichero;
                                        drowFilaMes["Fecha"] = DateTime.Parse("31/12/" + Annio);
                                        drowFilaMes["Presup_Mes"] = objHoja.Lineas[iCnt].Presup_DIC;
                                        drowFilaMes["GUID"] = strGUID;
                                        drowFilaMes["fechaAlta"] = DateTime.Now;
                                        drowFilaMes["usuarioAlta"] = strUsuario;
                                        drowFilaMes["fechaUltimaModif"] = DBNull.Value;
                                        drowFilaMes["usuarioUltimaModif"] = DBNull.Value;
                                        drowFilaMes["fechaBaja"] = DBNull.Value;
                                        drowFilaMes["usuarioBaja"] = DBNull.Value;

                                        dtableLineasPresupMes.Rows.Add(drowFilaMes);

                                        #endregion
                                    }
                                }

                                #endregion

                                #region INSERTA PRESUPUESTOS EN BBDD

                                // Realizamos una inserción masiva de los datos en la tabla de presupuestos
                                if (dtableLineasPresup.Rows.Count > 0)
                                {
                                    bResultado = objBBDD.BulkInsert(dtableLineasPresup, strTabla, strPK);

                                    if (!bResultado)
                                        throw new Exception("Error al insertar las lineas de presupuesto en la BBDD (bulkinsert)");
                                }

                                if (dtableLineasPresupMes.Rows.Count > 0)
                                {
                                    bResultado = objBBDD.BulkInsert(dtableLineasPresupMes, strTablaMes, strTablaMesPK);

                                    if (!bResultado)
                                        throw new Exception("Error al insertar las lineas de presupuesto mensuales en la BBDD (bulkinsert)");
                                }

                                #endregion

                                if (pbCargarMatrices)
                                {
                                    System.Data.DataTable dtableLineasMatriz = objBBDD.CrearEstructuraTabla(strTablaMatriz, strPKMatriz);

                                    #region RELLENA LA TABLA MATRIZ


                                    foreach (matrizPersona objMatrizPersona in _objMatriz.MatrizPersonasDpto)
                                    {
                                        foreach (KeyValuePair<int, double> kvpMatriz in objMatrizPersona.ValoresPorDpto)
                                        {
                                            DataRow drowFila = dtableLineasMatriz.NewRow();

                                            drowFila["idFichero"] = lIDFichero;
                                            drowFila["idTrabajador"] = objMatrizPersona.IDPersona;
                                            drowFila["idDepartamento"] = kvpMatriz.Key;
                                            drowFila["valor"] = kvpMatriz.Value;
                                            drowFila["fechaAlta"] = DateTime.Now;
                                            drowFila["usuarioAlta"] = strUsuario;
                                            drowFila["fechaUltimaModif"] = DBNull.Value;
                                            drowFila["usuarioUltimaModif"] = DBNull.Value;
                                            drowFila["fechaBaja"] = DBNull.Value;
                                            drowFila["usuarioBaja"] = DBNull.Value;

                                            dtableLineasMatriz.Rows.Add(drowFila);
                                        }
                                    }


                                    #endregion

                                    #region INSERTA MATRICES EN BBDD

                                    // Realizamos una inserción masiva de los datos en la tabla de matriz
                                    if (dtableLineasMatriz.Rows.Count > 0)
                                    {
                                        bResultado = objBBDD.BulkInsert(dtableLineasMatriz, strTablaMatriz, strPKMatriz);

                                        if (!bResultado)
                                            throw new Exception("Error al insertar las lineas de matrices en la BBDD (bulkinsert)");
                                    }

                                    #endregion

                                }

                                #endregion
                            }
                        }
                    }
                    catch (Exception exError)
                    {
                        ERROR = exError;
                        bResultado = false;
                    }
                    finally
                    {
                        if (bResultado)
                            objBBDD.CommitTransaction();
                        else
                            objBBDD.RollbackTransaction();
                    }
                }

                #region ACTUALIZA LOS ID DE LOS PRESUPUESTOS MENSUALES A TRAVÉS DE LOS GUID

                if (bResultado)
                {
                    try
                    {
                        strSQL = "UPDATE Tabla_PresupuestoMensual " +
                                 "SET Tabla_PresupuestoMensual.IdPresupuestoAnual = Tabla_PresupuestoAnual.IdPresupuestoAnual " +
                                 "FROM Tabla_PresupuestoAnual " +
                                 "INNER JOIN Tabla_PresupuestoMensual ON Tabla_PresupuestoAnual.GUID = Tabla_PresupuestoMensual.GUID " +
                                 "WHERE Tabla_PresupuestoAnual.IdFichero = @IdFichero";

                        lstParametros = new List<parametroBBDD>();

                        lstParametros.Add(new parametroBBDD() { Nombre = "@IdFichero", Valor = lIDFichero });

                        int iResultado = objBBDD.ExecuteSQL(strSQL, lstParametros);
                        bResultado = (iResultado > 0);

                        if (!bResultado)
                            throw new Exception("Error al actualizar los ID de los presupuestos a través de los GUID");
                    }
                    catch (Exception exError)
                    {
                        ERROR = exError;
                        bResultado = false;
                    }
                }

                #endregion

            }

            return bResultado;
        }

        private Dictionary<int, string> GetDepartamentos(bool pbEsParaMatrices = false)
        {
            Dictionary<int, string> dicDptos;

            try
            {
                string strQuery;

                dicDptos = new Dictionary<int, string>();

                if (pbEsParaMatrices)
                    strQuery = "SELECT * FROM Tabla_Departamento WHERE esParaMatriz = 1 AND fechaBaja IS NULL";
                else
                    strQuery = "SELECT * FROM Tabla_Departamento WHERE esParaPpto = 1 AND fechaBaja IS NULL";

                string strUsuario = GetUsuarioConectado();

                using (conexionSQLClient objBBDD = new conexionSQLClient(strUsuario, ConfigurationManager.ConnectionStrings["CODEUR"].ConnectionString))
                {
                    System.Data.DataTable dtableDptos = objBBDD.LoadTable(strQuery);

                    foreach (DataRow objFila in dtableDptos.Rows)
                    {
                        if (pbEsParaMatrices)
                            dicDptos.Add((int)objFila["idDepartamento"], objFila["nombreCorto"].ToString().Trim());
                        else
                            dicDptos.Add((int)objFila["idDepartamento"], objFila["nombreDpto"].ToString().Trim());
                    }
                }

            }
            catch (Exception exError)
            {
                ERROR = exError;
                dicDptos = null;
            }

            return dicDptos;
        }

        private List<trabajador> GetTrabajadores()
        {
            List<trabajador> lstTrabajadores = null;

            try
            {
                string strQuery;

                lstTrabajadores = new List<trabajador>();

                strQuery = "SELECT * FROM Tabla_Trabajador WHERE fechaBaja IS NULL";

                string strUsuario = GetUsuarioConectado();

                using (conexionSQLClient objBBDD = new conexionSQLClient(strUsuario, ConfigurationManager.ConnectionStrings["CODEUR"].ConnectionString))
                {
                    System.Data.DataTable dtableDptos = objBBDD.LoadTable(strQuery);

                    foreach (DataRow objFila in dtableDptos.Rows)
                    {
                        trabajador objTrabajador = new trabajador();
                        objTrabajador.ID = (long)objFila["idTrabajador"];
                        objTrabajador.Nombre = objFila["nombreCompleto"].ToString().Trim();                        
                        objTrabajador.EnOficina = (Convert.ToInt32(objFila["enOficina"]) > 0 ? true : false);
                        lstTrabajadores.Add(objTrabajador);
                    }
                }

            }
            catch (Exception exError)
            {
                ERROR = exError;
                lstTrabajadores = null;
            }

            return lstTrabajadores;
        }

        public string GetUsuarioConectado()
        {
            string strUsuario = null;

            try
            {
                if (HttpContext.Current != null && HttpContext.Current.User.Identity.Name != "")
                    strUsuario = HttpContext.Current.User.Identity.Name;
                else
                {
                    strUsuario = WindowsIdentity.GetCurrent().Name;
                }
            }
            catch
            {
                strUsuario = null;
            }
            return strUsuario;
        }

        private bool FicheroExiste(int piAnnio)
        {
            bool bResultado = false;

            string strQuery = "SELECT * FROM Tabla_Fichero WHERE Annio = " + Annio;
            string strUsuario = GetUsuarioConectado();

            using (conexionSQLClient objBBDD = new conexionSQLClient(strUsuario, ConfigurationManager.ConnectionStrings["CODEUR"].ConnectionString))
            {
                System.Data.DataTable dtableDptos = objBBDD.LoadTable(strQuery);

                if (dtableDptos.Rows.Count > 0)
                {
                    bResultado = true;
                }
            }

            return bResultado;
        }

        public void Dispose()
        {
            if (HojasExcel != null)
                HojasExcel.Clear();

            HojasExcel = null;
            GC.Collect();
        }

        private string QuitaAcentos(string pstrCadena)
        {
            string strResultado = pstrCadena;

            #region A
            
            if (pstrCadena.Contains('á'))
                strResultado = strResultado.Replace('á', 'a');

            if (pstrCadena.Contains('Á'))
                strResultado = strResultado.Replace('Á', 'A');

            #endregion

            #region E
            
            if (pstrCadena.Contains('é'))
                strResultado = strResultado.Replace('é', 'e');

            if (pstrCadena.Contains('É'))
                strResultado = strResultado.Replace('É', 'E');

            #endregion

            #region I

            if (pstrCadena.Contains('í'))
                strResultado = strResultado.Replace('í', 'i');

            if (pstrCadena.Contains('Í'))
                strResultado = strResultado.Replace('Í', 'I');

            #endregion

            #region O

            if (pstrCadena.Contains('ó'))
                strResultado = strResultado.Replace('ó', 'o');

            if (pstrCadena.Contains('Ó'))
                strResultado = strResultado.Replace('Ó', 'O');

            #endregion

            #region U

            if (pstrCadena.Contains('ú'))
                strResultado = strResultado.Replace('ú', 'u');

            if (pstrCadena.Contains('Ú'))
                strResultado = strResultado.Replace('Ú', 'U');

            #endregion

            return strResultado;
        }
    }


}
