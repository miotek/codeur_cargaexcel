﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODEURCargaExcelLIB
{
    public class hoja
    {
        public int IdDpto { get; set; }
        public string Nombre { get; set; }

        public long IDFichero { get; set; }        

        public List<linea> Lineas { get; set; }

        public hoja(string pstrNombre, int piID)
        {
            IdDpto = piID;
            Nombre = pstrNombre;
            Lineas = new List<linea>();
        }
    }
}
