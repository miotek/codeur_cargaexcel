﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODEURCargaExcelLIB
{
    public class linea
    {
        public bool EsTotal { get; set; }
        public string CtaFinanciera { get; set; }
        public string CtaAnalitica { get; set; }
        public string Concepto { get; set; }
        public double PresupuestoAnnio { get; set; }
        public double RealAnnioAnt { get; set; }
        public double Presup_ENE { get; set; }
        public double Presup_FEB { get; set; }
        public double Presup_MAR { get; set; }
        public double Presup_ABR { get; set; }
        public double Presup_MAY { get; set; }
        public double Presup_JUN { get; set; }
        public double Presup_JUL { get; set; }
        public double Presup_AGO { get; set; }
        public double Presup_SEP { get; set; }
        public double Presup_OCT { get; set; }
        public double Presup_NOV { get; set; }
        public double Presup_DIC { get; set; }
    }
}
