﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODEURCargaExcelLIB
{
    public class matrizPersona
    {
		#region PROPIEDADES

		public long IDPersona { get; set; }

		public string ApellidosNombre { get; set; }

		private Dictionary<int,double> _dicValoresPorDpto;
			
		public Dictionary<int, double> ValoresPorDpto
		{
			get { return _dicValoresPorDpto; }
			set { _dicValoresPorDpto = value; }
		}

		#endregion

		public matrizPersona()
		{
			ValoresPorDpto = new Dictionary<int, double>();
		}
	}
}
