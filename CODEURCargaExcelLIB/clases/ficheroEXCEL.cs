﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace CODEURCargaExcelLIB
{
    public class ficheroEXCEL
    {
        const string strCADENA_CONEXION_XLSX = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Extended Properties=\"Excel 12.0\";";
        //const string strCADENA_CONEXION_XLS = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}; Extended Properties= Excel 8.0;";

        const string strCADENA_CONEXION_XLS = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=YES\";";
        
        #region PROPIEDADES        

        private string _strCadenaConexion;
        public string CadenaConexion
        {
            get { return _strCadenaConexion; }
            set { _strCadenaConexion = value; }
        }

        private string _strFicheroExcel;
        public string FicheroExcel
        {
            get { return _strFicheroExcel; }
            set { _strFicheroExcel = value; }
        }
        
        private List<string> _lstHojasExcel;
        public List<string> HojasExcel
        {
            get { return _lstHojasExcel; }
            set { _lstHojasExcel = value; }
        }

        private string _strHojaSeleccionada;
        public string HojaSeleccionada
        {
            get { return _strHojaSeleccionada; }
            set { _strHojaSeleccionada = value; }
        }
        
        public Exception Error { get; set; }

        #endregion

        #region METODOS        

        public ficheroEXCEL(string pstrFicheroTarifas)
        {
            try
            {
                _strFicheroExcel = pstrFicheroTarifas;
                _lstHojasExcel = new List<string>();

                if (File.Exists(_strFicheroExcel))
                {
                    FileInfo objFichero = new FileInfo(_strFicheroExcel);

                    if (objFichero.Extension.ToUpper().Equals(".XLSX"))
                        _strCadenaConexion = String.Format(strCADENA_CONEXION_XLSX, _strFicheroExcel);
                    else if (objFichero.Extension.ToUpper().Equals(".XLS"))
                        _strCadenaConexion = String.Format(strCADENA_CONEXION_XLS, _strFicheroExcel);

                    if (_strCadenaConexion.Length > 0)
                    {
                        OleDbConnection oledbconConexion = new OleDbConnection(_strCadenaConexion);
                        oledbconConexion.Open();
                        DataTable dtableHojasExcel = oledbconConexion.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                        string strValor;

                        foreach (DataRow drowHoja in dtableHojasExcel.Rows)
                        {
                            strValor = drowHoja[2].ToString().Replace("'", "");
                            if (strValor.EndsWith("$"))
                                _lstHojasExcel.Add(strValor.Substring(0, strValor.Length - 1));

                            CargarFichero(oledbconConexion, _lstHojasExcel.Last());
                        }
                        oledbconConexion.Close();
                    }
                }
            }
            catch (Exception exError)
            {
                Error = exError;
            }
        }

        public virtual void CargarFichero(OleDbConnection poledbconConexion, string pstrHojaExcel)
        {
            DataSet dsetRepositorio;
            DataTable dtableResultado = null;

            try
            {
                if (poledbconConexion != null && poledbconConexion.State == ConnectionState.Open)
                {
                    dsetRepositorio = new DataSet();
                    DataTable dtableTarifas = new DataTable();

                    dsetRepositorio.Tables.Add(dtableTarifas);

                    using (OleDbCommand oledbcomSQL = new OleDbCommand())
                    {
                        oledbcomSQL.Connection = poledbconConexion;
                        OleDbDataAdapter odbcdaAdapter = new OleDbDataAdapter(oledbcomSQL);

                        string strFicheroQuery = String.Empty;
                        string strQuery = String.Empty;

                        oledbcomSQL.CommandText = String.Format("SELECT * FROM [{0}$] ", pstrHojaExcel);

                        odbcdaAdapter.Fill(dtableTarifas);

                        dtableTarifas.Columns.Add(new DataColumn("NumLinea", typeof(Int32)));

                        for (int iCnt = 0; iCnt < dtableTarifas.Rows.Count; iCnt++)
                            dtableTarifas.Rows[iCnt]["NumLinea"] = iCnt + 2;


                        dtableResultado = dtableTarifas;
                    }
                }
            }
            catch (Exception exError)
            {
                Error = exError;
            }

            //DatosHoja = dtableResultado;

        }

        #endregion
    }
}
