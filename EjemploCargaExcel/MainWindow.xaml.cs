﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using CODEURCargaExcelLIB;

namespace WpfApp1
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ficheroEXCEL _objFichero;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnAbrir_Click(object sender, RoutedEventArgs e)
        {
            int iAnnio = 2000;

            OpenFileDialog objAbrirFichero = new OpenFileDialog();
            objAbrirFichero.Filter = "Excel (*.xlsx)|*.xlsx|Excel 97 (*.xls)|*.xls|All files (*.*)|*.*";

            if (objAbrirFichero.ShowDialog() == true && txtAnnio.Text.Length > 0)
            {
                txtNombreFicheo.Text = objAbrirFichero.FileName;
                if (int.TryParse(txtAnnio.Text, out iAnnio))
                {
                    #region POR OLEDB
                    //_objFichero = new ficheroEXCEL(txtNombreFicheo.Text);

                    //if (_objFichero.Error != null)
                    //    MessageBox.Show(_objFichero.Error.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    //else
                    //{
                    //    StringBuilder sb = new StringBuilder();
                    //    foreach (string strNombreHoja in _objFichero.HojasExcel)
                    //    {
                    //        sb.AppendLine(strNombreHoja);
                    //    }

                    //    txtHojasExcel.Text = sb.ToString();
                    //    //cboHojas.ItemsSource = null;
                    //    //cboHojas.ItemsSource = _objFichero.HojasExcel;
                    //}
                    #endregion

                    txtHojasExcel.Text = "Inicio: " + DateTime.Now.ToLongTimeString();

                    using (interopExcel objExcel = new interopExcel(iAnnio, txtNombreFicheo.Text))
                    {
                        bool bResultado = objExcel.AbrirExcel();

                        if (bResultado)
                            bResultado = objExcel.CargaExcelPresupuesto();

                        if (bResultado && (bool)chkCargaMatrices.IsChecked)
                            bResultado = objExcel.CargaExcelMatrices();

                        if (bResultado)
                            bResultado = objExcel.CerrarExcel();

                        if (bResultado)
                            bResultado = objExcel.InsertaDatosExcelEnBBDD(true, (bool)chkCargaMatrices.IsChecked);

                        txtHojasExcel.Text += "\nFin: " + DateTime.Now.ToLongTimeString();

                        MessageBox.Show("El resultado de la operación es: " + bResultado);
                    }
                }
                else
                    MessageBox.Show("El año debe ser numérico");
            }
        }
    }
}
