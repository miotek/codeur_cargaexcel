﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ConexionBBDD
{
    public enum TipoConexion
    { 
        CadenaConexion, FicheroUDL
    }

    public enum Proveedor
    { 
        OleDB, SQLClient
    }

    public abstract class conexionBBDD
    {

        #region PROPIEDADES

        private string _strCadenaConexion;
        public string CadenaConexion
        {
            get { return _strCadenaConexion; }
            set { _strCadenaConexion = value; }
        }

        private string _strUsuarioConectado;
        public string UsuarioConectado
        {
            get { return _strUsuarioConectado; }
            set { _strUsuarioConectado = value; }
        }

        private Exception _exError;
        public Exception Error
        {
            get { return _exError; }
            set { _exError = value; }
        }

        #endregion

        #region METODOS

        public abstract bool OpenDB();

        public abstract bool CloseDB();

        public abstract int ExecuteSQL(string pstrSQL, List<parametroBBDD> plstParametros = null);

        public abstract object ExecuteEscalar(string pstrSQL, List<parametroBBDD> plstParametros = null);

        public abstract DataTable LoadTable(string pstrSQL, List<parametroBBDD> plstParametros = null);

        public abstract bool InitTransaction(IsolationLevel pobjIsolationLevel);

        public abstract bool CommitTransaction();

        public abstract bool RollbackTransaction();

        #endregion

    }
}
