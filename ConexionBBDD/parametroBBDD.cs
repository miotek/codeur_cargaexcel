﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace ConexionBBDD
{
    public class parametroBBDD
    {
        private string _strNombre;
        public string Nombre
        {
            get { return _strNombre; }
            set { _strNombre = value; }
        }
        
        private OleDbType _TipoOleDB;
        public OleDbType TipoOleDB
        {
            get { return _TipoOleDB; }
            set { _TipoOleDB = value; }
        }
        
        private SqlDbType _TipoSQLClient;
        public SqlDbType TipoSQLClient
        {
            get { return _TipoSQLClient; }
            set { _TipoSQLClient = value; }
        }
        
        private object _objValor;
        public object Valor 
        {
            get { return _objValor; }
                
            set 
            {
                _objValor = value;

                try
                {

                    if (_objValor != null)
                    {

                        #region ENTEROS

                        if (_objValor.GetType() == typeof(byte))
                        {
                            _TipoOleDB = OleDbType.TinyInt;
                            _TipoSQLClient = SqlDbType.TinyInt;
                        }

                        if (_objValor.GetType() == typeof(Int16))
                        {
                            _TipoOleDB = OleDbType.SmallInt;
                            _TipoSQLClient = SqlDbType.SmallInt;
                        }

                        if (_objValor.GetType() == typeof(Int32))
                        {
                            _TipoOleDB = OleDbType.Integer;
                            _TipoSQLClient = SqlDbType.Int;
                        }

                        if (_objValor.GetType() == typeof(Int64))
                        {
                            _TipoOleDB = OleDbType.BigInt;
                            _TipoSQLClient = SqlDbType.BigInt;
                        }

                        #endregion

                        #region DECIMALES

                        if (_objValor.GetType() == typeof(Single))
                        {
                            _TipoOleDB = OleDbType.Single;
                            _TipoSQLClient = SqlDbType.Real;
                        }

                        if (_objValor.GetType() == typeof(decimal))
                        {
                            _TipoOleDB = OleDbType.Decimal;
                            _TipoSQLClient = SqlDbType.Decimal;
                        }

                        if (_objValor.GetType() == typeof(double))
                        {
                            _TipoOleDB = OleDbType.Double;
                            _TipoSQLClient = SqlDbType.Float;
                        }



                        #endregion

                        #region CADENAS

                        if (_objValor.GetType() == typeof(char))
                        {
                            _TipoOleDB = OleDbType.Char;
                            _TipoSQLClient = SqlDbType.Char;
                        }

                        if (_objValor.GetType() == typeof(string))
                        {
                            _TipoOleDB = OleDbType.VarChar;
                            _TipoSQLClient = SqlDbType.VarChar;
                        }

                        #endregion

                        #region OTROS

                        if (_objValor.GetType() == typeof(bool))
                        {
                            _TipoOleDB = OleDbType.Boolean;
                            _TipoSQLClient = SqlDbType.Bit;
                        }

                        if (_objValor.GetType() == typeof(DateTime))
                        {
                            _TipoOleDB = OleDbType.Date;
                            _TipoSQLClient = SqlDbType.SmallDateTime;
                        }

                        #endregion

                    }
                    else
                        _objValor = DBNull.Value;
                }
                catch
                {

                }
            }
        }
        
    }
}
