﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ConexionBBDD
{
    public class conexionSQLClient : conexionBBDD, IDisposable
    {

        #region PROPIEDADES

        private SqlConnection _sqlconConexion;
        public SqlConnection Conexion
        {
            get { return _sqlconConexion; }
        }

        private SqlTransaction _sqltransTransaccion;
        public SqlTransaction Transaccion
        {
            get { return _sqltransTransaccion; }
        }

        private SqlCommand _sqlcomComandoBBDD;
        public SqlCommand ComandoBBDD
        {
            get { return _sqlcomComandoBBDD; }
            set { _sqlcomComandoBBDD = value; }
        }
        
        #endregion

        #region CONSTRUCTORES

        public conexionSQLClient(string pstrUsuarioConectado, string pstrCadenaConexion)
        {
            try
            {
                _sqlconConexion = new SqlConnection(pstrCadenaConexion);                
                UsuarioConectado = pstrUsuarioConectado;
            }
            catch (Exception exError)
            {
                Error = exError;
            }
        }

        #endregion

        #region METODOS

        public override bool OpenDB() 
        {
            bool bResultado = true;

            try
            {
                if (_sqlconConexion.ConnectionString.Length == 0)
                    // Abre la conexión en función de los parámetros recibidos
                    _sqlconConexion.ConnectionString = CadenaConexion;

                // Abre la conexión con la BD
                _sqlconConexion.Open();
            }
            catch (Exception exError)
            {
                bResultado = false;
                Error = exError;
            }

            return bResultado;
        }

        public override bool CloseDB() 
        {
            bool bResultado = true;

            try
            {
                _sqlconConexion.Close();
            }
            catch (Exception exError)
            {
                bResultado = false;
                Error = exError;
                _sqlconConexion = null;
            }

            return bResultado;
        }

        public override int ExecuteSQL(string pstrSQL, List<parametroBBDD> plstParametros = null) 
        {
            int iRegistrosAfectados;
            bool bIsConnectionOpenHere = false;

            if (_sqlconConexion.State == ConnectionState.Closed)
            {
                OpenDB();
                bIsConnectionOpenHere = true;
            }

            try
            {
                using (_sqlcomComandoBBDD = new SqlCommand())
                {
                    _sqlcomComandoBBDD.Connection = _sqlconConexion;
                    _sqlcomComandoBBDD.CommandTimeout = 999999;

                    if (_sqltransTransaccion != null)
                        _sqlcomComandoBBDD.Transaction = _sqltransTransaccion;

                    if (pstrSQL != null)
                        _sqlcomComandoBBDD.CommandText = pstrSQL;

                    if (plstParametros != null)
                    {
                        foreach (parametroBBDD pobjParametro in plstParametros)
                        {
                            SqlParameter sqlparParametro = new SqlParameter(pobjParametro.Nombre, pobjParametro.TipoSQLClient);
                            sqlparParametro.Value = pobjParametro.Valor;
                            _sqlcomComandoBBDD.Parameters.Add(sqlparParametro);
                        }
                    }

                    iRegistrosAfectados = _sqlcomComandoBBDD.ExecuteNonQuery();
                }
            }
            catch (Exception exError)
            {
                Error = exError;
                iRegistrosAfectados = -1;
            }
            finally
            {
                if (bIsConnectionOpenHere && _sqlconConexion.State == ConnectionState.Open)
                {
                    _sqlconConexion.Close();
                }
            }

            return iRegistrosAfectados;
        }

        public override object ExecuteEscalar(string pstrSQL, List<parametroBBDD> plstParametros = null)
        {
            object objRegistrosAfectados;
            bool bIsConnectionOpenHere = false;

            if (_sqlconConexion.State == ConnectionState.Closed)
            {
                OpenDB();
                bIsConnectionOpenHere = true;
            }

            try
            {
                using (_sqlcomComandoBBDD = new SqlCommand())
                {
                    _sqlcomComandoBBDD.Connection = _sqlconConexion;
                    _sqlcomComandoBBDD.CommandTimeout = 999999;

                    if (_sqltransTransaccion != null)
                        _sqlcomComandoBBDD.Transaction = _sqltransTransaccion;

                    if (pstrSQL != null)
                        _sqlcomComandoBBDD.CommandText = pstrSQL;

                    if (plstParametros != null)
                    {
                        foreach (parametroBBDD pobjParametro in plstParametros)
                        {
                            SqlParameter sqlparParametro = new SqlParameter(pobjParametro.Nombre, pobjParametro.TipoSQLClient);
                            sqlparParametro.Value = pobjParametro.Valor;
                            _sqlcomComandoBBDD.Parameters.Add(sqlparParametro);
                        }
                    }

                    objRegistrosAfectados = _sqlcomComandoBBDD.ExecuteScalar();
                }
            }
            catch (Exception exError)
            {
                Error = exError;
                objRegistrosAfectados = null;
            }
            finally
            {
                if (bIsConnectionOpenHere && _sqlconConexion.State == ConnectionState.Open)
                {
                    _sqlconConexion.Close();
                }
            }

            return objRegistrosAfectados;
        }

        public override DataTable LoadTable(string pstrSQL, List<parametroBBDD> plstParametros = null)
        {
            DataTable dtableResultado = null;
            bool bIsConnectionOpenHere = false;

            if (_sqlconConexion.State == ConnectionState.Closed)
            {
                OpenDB();
                bIsConnectionOpenHere = true;
            }

            try
            {
                using (_sqlcomComandoBBDD = new SqlCommand())
                {
                    _sqlcomComandoBBDD.Connection = _sqlconConexion;

                    if (_sqltransTransaccion != null)
                        _sqlcomComandoBBDD.Transaction = _sqltransTransaccion;
                    
                    if (pstrSQL != null)
                        _sqlcomComandoBBDD.CommandText = pstrSQL;

                    if (plstParametros != null)
                    {
                        foreach (parametroBBDD pobjParametro in plstParametros)
                        {
                            SqlParameter sqlparParametro = new SqlParameter(pobjParametro.Nombre, pobjParametro.TipoSQLClient);
                            sqlparParametro.Value = pobjParametro.Valor;
                            _sqlcomComandoBBDD.Parameters.Add(sqlparParametro);
                        }
                    }

                    SqlDataReader sqldrTabla = _sqlcomComandoBBDD.ExecuteReader();

                    dtableResultado = ConvertReaderToTable(sqldrTabla);

                    sqldrTabla.Close();
                }
            }
            catch (Exception exError)
            {
                Error = exError;
                dtableResultado = null;
            }
            finally
            {
                if (_sqlconConexion != null && bIsConnectionOpenHere && _sqlconConexion.State == ConnectionState.Open)
                {
                    _sqlconConexion.Close();
                }
            }

            return dtableResultado;
        }
        
        public override bool InitTransaction(IsolationLevel pobjIsolationLevel = IsolationLevel.ReadUncommitted) 
        {
            bool bResultado = true;

            try
            {
                if (_sqlconConexion.State == ConnectionState.Closed)
                    OpenDB();

                _sqltransTransaccion = _sqlconConexion.BeginTransaction(pobjIsolationLevel);

                return true;
            }
            catch (Exception exError)
            {
                bResultado = false;
                Error = exError;
            }
            return bResultado;
        }

        public override bool CommitTransaction() 
        {
            bool bResultado = true;

            try
            {
                if (_sqlconConexion.State == ConnectionState.Open)
                    _sqltransTransaccion.Commit();

                return true;
            }
            catch (Exception exError)
            {
                Error = exError;
                bResultado= false;
            }

            return bResultado;
        }

        public override bool RollbackTransaction() 
        {
            bool bResultado = true;

            try
            {
                if (_sqlconConexion.State == ConnectionState.Open)
                    _sqltransTransaccion.Rollback();

                return true;
            }
            catch (Exception exError)
            {
                Error = exError;
                bResultado = false;
            }

            return bResultado;
        }

        public bool BulkInsert(DataTable pdtableDatos, string pstrNombreTabla, string pstrNombreCampoIdentity = null) 
        {
            bool bResultado = true;

            bool bIsConnectionOpenHere = false;

            if (_sqlconConexion.State == ConnectionState.Closed)
            {
                OpenDB();
                bIsConnectionOpenHere = true;
            }

            try
            {
                //using (SqlBulkCopy objBK = new SqlBulkCopy(_sqlconConexion, SqlBulkCopyOptions.KeepIdentity, _sqltransTransaccion))
                using (SqlBulkCopy objBK = new SqlBulkCopy(_sqlconConexion, SqlBulkCopyOptions.TableLock, _sqltransTransaccion))
                {                    
                    objBK.BulkCopyTimeout = 0;

                    objBK.BatchSize = 40000;
                    
                    foreach (DataColumn column in pdtableDatos.Columns)
                    {
                        if (column.ColumnName != pstrNombreCampoIdentity)
                        {
                            objBK.ColumnMappings.Add(column.ColumnName, column.ColumnName);
                        }
                    }

                    objBK.DestinationTableName = pstrNombreTabla;

                    objBK.WriteToServer(pdtableDatos);
                    objBK.Close();
                }

            }
            catch (Exception exError)
            {
                Error = exError;
                bResultado = false;
            }             
            finally
            {
                if (_sqlconConexion != null && bIsConnectionOpenHere && _sqlconConexion.State == ConnectionState.Open)
                {
                    _sqlconConexion.Close();
                }
            }

            return bResultado;
        }

        public DataTable CrearEstructuraTabla(string pstrNombreTabla, string pstrNombreCampoIdentity = null)
        {
            DataTable dtableDatos = null;
            //string strNombreTabla = String.Empty;

            try
            {
                string strQuery = "select column_name, data_type " +
                                  "from information_schema.columns " +
                                  "where table_name = '{0}' " +
                                  "order by ordinal_position ";

                strQuery = String.Format(strQuery, pstrNombreTabla);

                // Lanzamos la query para obtener los nombres y tipos de las columnas de la tabla que corresponda
                using (DataTable dtableColumnas = LoadTable(strQuery))
                {

                    dtableDatos = new DataTable();
                    Type TipoColumna;

                    for (int i = 0; i < dtableColumnas.Rows.Count; i++) 
                    {
                        #region SELECCIONAR TIPO DE DATO PARA LA COLUMNA

                        switch (dtableColumnas.Rows[i][1].ToString())
                        {
                            case "text":
                            case "nvarchar":
                            case "varchar":
                            case "char":
                                TipoColumna = typeof(string);
                                break;

                            case "float":
                                TipoColumna = typeof(float);
                                break;

                            case "decimal":
                                TipoColumna = typeof(decimal);
                                break;

                            case "datetime":
                            case "smalldatetime":
                                TipoColumna = typeof(DateTime);
                                break;

                            case "smallint":
                                TipoColumna = typeof(short);
                                break;

                            case "int":
                                TipoColumna = typeof(int);
                                break;

                            case "bigint":
                                TipoColumna = typeof(long);
                                break;

                            case "bit":
                                TipoColumna = typeof(bool);
                                break;

                            case "timestamp":
                                TipoColumna = typeof(TimeSpan);
                                break;

                            default:
                                TipoColumna = typeof(object);
                                break;
                        }

                        #endregion

                        if (pstrNombreCampoIdentity != null)
                        {
                            if (dtableColumnas.Rows[i][0].ToString() != pstrNombreCampoIdentity)
                                dtableDatos.Columns.Add(dtableColumnas.Rows[i][0].ToString(), TipoColumna);
                        }
                        else
                            dtableDatos.Columns.Add(dtableColumnas.Rows[i][0].ToString(), TipoColumna);
                    }
                }

            }
            catch (Exception exError)
            {
                Error = exError;
                dtableDatos = null;
            }

            return dtableDatos;
        }

        private DataTable ConvertReaderToTable(SqlDataReader psqldrDatos)
        {            
            DataTable dtableResultado;

            try
            {
                DataTable dtableSchema = psqldrDatos.GetSchemaTable();
                DataColumn[] dcolColumns = new DataColumn[dtableSchema.Rows.Count];
                DataColumn dcolColumn;

                // Build the schema for the table that will contain the data
                for (int iCnt = 0; iCnt < dcolColumns.Length; ++iCnt)
                {
                    dcolColumn = new DataColumn();
                    dcolColumn.AllowDBNull = (bool)(dtableSchema.Rows[iCnt]["AllowDBNull"]);
                    dcolColumn.AutoIncrement = (bool)(dtableSchema.Rows[iCnt]["IsAutoIncrement"]);
                    dcolColumn.ColumnName = (String)(dtableSchema.Rows[iCnt]["ColumnName"]);
                    dcolColumn.DataType = (System.Type)(dtableSchema.Rows[iCnt]["DataType"]);
                    if (dcolColumn.DataType == typeof(String))
                        dcolColumn.MaxLength = (int)(dtableSchema.Rows[iCnt]["ColumnSize"]);
                    //					dcolColumn.ReadOnly = (bool)(dtableSchema.Rows[iCnt]["ReadOnly"]);
                    dcolColumn.Unique = (bool)(dtableSchema.Rows[iCnt]["IsUnique"]);
                    dcolColumns[iCnt] = dcolColumn;
                }

                dtableResultado = new DataTable();
                DataRow drowRecord;

                dtableResultado.Columns.AddRange(dcolColumns);

                while (psqldrDatos.Read())
                {
                    drowRecord = dtableResultado.NewRow();

                    for (int iCnt = 0; iCnt <= dcolColumns.GetUpperBound(0); iCnt++)
                        drowRecord[iCnt] = psqldrDatos[iCnt];

                    dtableResultado.Rows.Add(drowRecord);
                }

                return dtableResultado;
            }
            catch (Exception exError)
            {
                Error = exError;
                dtableResultado = null;
            }

            return dtableResultado;

        }

        void IDisposable.Dispose()
        {
            if (_sqlconConexion != null && _sqlconConexion.State == ConnectionState.Open)
                _sqlconConexion.Close();

            _sqlconConexion.Dispose();
        }

        #endregion

    }
}
