﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Configuration;

namespace ConexionBBDD
{
    public class conexionOLEDB : conexionBBDD, IDisposable
    {        

        #region PROPIEDADES

        private OleDbConnection _oledbconConexion;
        public OleDbConnection Connection
        {
            get { return _oledbconConexion; }
        }

        private OleDbTransaction _oledbtransTransaccion;
        public OleDbTransaction Transaction
        {
            get { return _oledbtransTransaccion; }
        }

        private OleDbCommand _oledbcomComandoOLEDB;
        public OleDbCommand ComandoBBDD
        {
            get { return _oledbcomComandoOLEDB; }
            set { _oledbcomComandoOLEDB = value; }
        }        
        
        #endregion

        #region CONSTRUCTORES

        public conexionOLEDB(string pstrUsuarioConectado, string pstrCadenaConexion, TipoConexion pobjTipoConexion = TipoConexion.CadenaConexion)
        {
            try
            {
                if (pobjTipoConexion == TipoConexion.CadenaConexion)
                    _oledbconConexion = new OleDbConnection(pstrCadenaConexion);
                
                else if (pobjTipoConexion == TipoConexion.FicheroUDL)
                    _oledbconConexion = new OleDbConnection("FILE NAME=" + pstrCadenaConexion);
                

                UsuarioConectado = pstrUsuarioConectado;
            }
            catch (Exception exError)
            {
                Error = exError;
            }
        }
	    
		#endregion
    	
		#region METODOS

        public override bool OpenDB()
        {
            bool bResultado = true;

            try
            {
                if (_oledbconConexion.ConnectionString.Length == 0)
                    // Abre la conexión en función de los parámetros recibidos
                    _oledbconConexion.ConnectionString = CadenaConexion;

                // Abre la conexión con la BD
                _oledbconConexion.Open();
            }
            catch (Exception exError)
            {
                bResultado = false;
                Error = exError;
            }

            return bResultado;
        }

        public override bool CloseDB()
		{
            bool bResultado = true;

            try
            {
                _oledbconConexion.Close();
            }
            catch (Exception exError)
            {
                bResultado = false;
                Error = exError;
                _oledbconConexion = null;
            }

            return bResultado;
		}

        public override int ExecuteSQL(string pstrSQL, List<parametroBBDD> plstParametros = null)
		{
            int iRegistrosAfectados;
            bool bIsConnectionOpenHere = false;

            if (_oledbconConexion.State == ConnectionState.Closed)
            {
                OpenDB();
                bIsConnectionOpenHere = true;
            }

            try
            {
                using (_oledbcomComandoOLEDB = new OleDbCommand())
                {
                    _oledbcomComandoOLEDB.Connection = _oledbconConexion;

                    if (_oledbtransTransaccion != null)
                        _oledbcomComandoOLEDB.Transaction = _oledbtransTransaccion;

                    if (pstrSQL != null)
                        _oledbcomComandoOLEDB.CommandText = pstrSQL;

                    if (plstParametros != null)
                    {
                        foreach (parametroBBDD pobjParametro in plstParametros)
                        {
                            OleDbParameter oledbparParametro = new OleDbParameter(pobjParametro.Nombre, pobjParametro.TipoOleDB);
                            oledbparParametro.Value = pobjParametro.Valor;
                            _oledbcomComandoOLEDB.Parameters.Add(pobjParametro);
                        }
                    }

                    iRegistrosAfectados = _oledbcomComandoOLEDB.ExecuteNonQuery();
                }
            }
            catch (Exception exError)
            {
                Error = exError;
                iRegistrosAfectados = -1;
            }
            finally
            {
                if (bIsConnectionOpenHere && _oledbconConexion.State == ConnectionState.Open)
                {
                    _oledbconConexion.Close();
                }
            }

            return iRegistrosAfectados;
		}

        public override object ExecuteEscalar(string pstrSQL, List<parametroBBDD> plstParametros = null)
        {
            object objRegistrosAfectados;
            bool bIsConnectionOpenHere = false;

            if (_oledbconConexion.State == ConnectionState.Closed)
            {
                OpenDB();
                bIsConnectionOpenHere = true;
            }

            try
            {
                using (_oledbcomComandoOLEDB = new OleDbCommand())
                {
                    _oledbcomComandoOLEDB.Connection = _oledbconConexion;

                    if (_oledbtransTransaccion != null)
                        _oledbcomComandoOLEDB.Transaction = _oledbtransTransaccion;

                    if (pstrSQL != null)
                        _oledbcomComandoOLEDB.CommandText = pstrSQL;

                    if (plstParametros != null)
                    {
                        foreach (parametroBBDD pobjParametro in plstParametros)
                        {
                            OleDbParameter oledbparParametro = new OleDbParameter(pobjParametro.Nombre, pobjParametro.TipoOleDB);
                            oledbparParametro.Value = pobjParametro.Valor;
                            _oledbcomComandoOLEDB.Parameters.Add(pobjParametro);
                        }
                    }

                    objRegistrosAfectados = _oledbcomComandoOLEDB.ExecuteNonQuery();
                }
            }
            catch (Exception exError)
            {
                Error = exError;
                objRegistrosAfectados = null;
            }
            finally
            {
                if (bIsConnectionOpenHere && _oledbconConexion.State == ConnectionState.Open)
                {
                    _oledbconConexion.Close();
                }
            }

            return objRegistrosAfectados;
        }

        public override DataTable LoadTable(string pstrSQL, List<parametroBBDD> plstParametros = null)
		{
            DataTable dtableResultado = null;
            bool bIsConnectionOpenHere = false;

            if (_oledbconConexion.State == ConnectionState.Closed)
            {
                OpenDB();
                bIsConnectionOpenHere = true;
            }

            try
            {
                using (_oledbcomComandoOLEDB = new OleDbCommand())
                {
                    _oledbcomComandoOLEDB.Connection = _oledbconConexion;
                    _oledbcomComandoOLEDB.CommandTimeout = 999999;

                    if (_oledbtransTransaccion != null)
                        _oledbcomComandoOLEDB.Transaction = _oledbtransTransaccion;

                    if (pstrSQL != null)
                        _oledbcomComandoOLEDB.CommandText = pstrSQL;

                    if (plstParametros != null)
                    {
                        foreach (parametroBBDD objParametro in plstParametros)
                        {
                            OleDbParameter oledbparParametro = new OleDbParameter();
                            oledbparParametro.ParameterName = objParametro.Nombre;
                            oledbparParametro.Value = objParametro.Valor;
                            oledbparParametro.OleDbType = objParametro.TipoOleDB;
                            _oledbcomComandoOLEDB.Parameters.Add(oledbparParametro);
                        }
                    }

                    OleDbDataReader sqldrTabla = _oledbcomComandoOLEDB.ExecuteReader();

                    dtableResultado = ConvertReaderToTable(sqldrTabla);

                    sqldrTabla.Close();                     
                }
            }
            catch (Exception exError)
            {
                Error = exError;
                dtableResultado = null;
            }
            finally
            {
                if (_oledbconConexion != null && bIsConnectionOpenHere && _oledbconConexion.State == ConnectionState.Open)
                {
                    _oledbconConexion.Close();
                }
            }

            return dtableResultado;
		}

		/*

		isolation levels:
		
		RepeatableRead –	Slowly getting more isolated. In this case, a shared lock is applied on all data queried within a transaction. 
									This means that no other transaction can alter the data used in your transaction. This prevents the case where 
									data you had queried once changes on subsequent queries. It does not, though, prevent the case where 
									rows are added to the table that may be returned in subsequent queries. 
									
		Serializable –			Locks are placed on ranges of the tables you are using, preventing other users from changing your data 
									or adding new rows underneath you. This is the most isolated isolation level, but it does come with the 
									drawback of locking more data than your transaction may strictly need. 

			 
		In SQL Server 2005, a new isolation level will be added: snapshot isolation. 
		In snapshot isolation, rows are versioned once they are accessed in a transaction. 
		This essentially means that once a transaction accesses a set of values, they are guaranteed to remain the same 
		until you commit or rollback the transaction. 
		Other transactions starting in the middle of the first will get a ‘copy’ of the original database to operate on. 
		Before any transaction commits, though, SQL Server will test to ensure that the original data they were 
		operating on is the same as the current data in the database. 
		If this is the case, the transaction will commit. Otherwise, the transaction will be rolled back and the user 
		will have to try the batch once again.
		 
		*/

        public override bool InitTransaction(IsolationLevel pobjIsolationLevel = IsolationLevel.ReadUncommitted)
		{
            bool bResultado = true;

            try
            {
                if (_oledbconConexion.State == ConnectionState.Closed)
                    OpenDB();

                _oledbtransTransaccion = _oledbconConexion.BeginTransaction(pobjIsolationLevel);

                return true;
            }
            catch (Exception exError)
            {
                bResultado = false;
                Error = exError;
            }

            return bResultado;
		}

        public override bool CommitTransaction()
		{
            bool bResultado = true;

            try
            {
                if (_oledbconConexion.State == ConnectionState.Open)
                    _oledbtransTransaccion.Commit();

                return true;
            }
            catch (Exception exError)
            {
                Error = exError;
                bResultado = false;
            }

            return bResultado;
		}

        public override bool RollbackTransaction()
		{
            bool bResultado = true;

            try
            {
                if (_oledbconConexion.State == ConnectionState.Open)
                    _oledbtransTransaccion.Rollback();

                return true;
            }
            catch (Exception exError)
            {
                Error = exError;
                bResultado = false;
            }

            return bResultado;
		}

        private DataTable ConvertReaderToTable(OleDbDataReader poledbrObject)
		{
            DataTable dtableResult;

			try
			{
				DataTable dtableSchema = poledbrObject.GetSchemaTable();
				DataColumn[] dcolColumns = new DataColumn[dtableSchema.Rows.Count];
				DataColumn dcolColumn;

				// Build the schema for the table that will contain the data
				for (int iCnt = 0; iCnt < dcolColumns.Length; ++iCnt)
				{
					dcolColumn = new DataColumn();
					dcolColumn.AllowDBNull = (bool)(dtableSchema.Rows[iCnt]["AllowDBNull"]);
					dcolColumn.AutoIncrement = (bool)(dtableSchema.Rows[iCnt]["IsAutoIncrement"]);
					dcolColumn.ColumnName = (String)(dtableSchema.Rows[iCnt]["ColumnName"]);
					dcolColumn.DataType = (System.Type)(dtableSchema.Rows[iCnt]["DataType"]);
					if (dcolColumn.DataType == typeof(String))
						dcolColumn.MaxLength = (int)(dtableSchema.Rows[iCnt]["ColumnSize"]);
//					dcolColumn.ReadOnly = (bool)(dtableSchema.Rows[iCnt]["ReadOnly"]);
					dcolColumn.Unique = (bool)(dtableSchema.Rows[iCnt]["IsUnique"]);
					dcolColumns[iCnt] = dcolColumn;
				}

				dtableResult = new DataTable();
				DataRow drowRecord;

				dtableResult.Columns.AddRange(dcolColumns);

				while (poledbrObject.Read())
				{
					drowRecord = dtableResult.NewRow();
					
					for (int iCnt = 0; iCnt <= dcolColumns.GetUpperBound(0); iCnt++)
						drowRecord[iCnt] = poledbrObject[iCnt];
					
					dtableResult.Rows.Add(drowRecord);
				}

				return dtableResult;
			}
			catch(Exception exError)
			{
				Error = exError;
                dtableResult = null;
			}

            return dtableResult;
		}

        void IDisposable.Dispose()
        {
            if (_oledbconConexion != null && _oledbconConexion.State == ConnectionState.Open)
                _oledbconConexion.Close();

            _oledbconConexion.Dispose();
        }

		#endregion

    }
}
